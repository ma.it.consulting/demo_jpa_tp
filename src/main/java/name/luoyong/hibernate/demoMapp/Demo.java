package name.luoyong.hibernate.demoMapp;

import name.luoyong.hibernate.basic.entity.heritage.Formation;
import name.luoyong.hibernate.basic.entity.heritage.FormationInter;
import name.luoyong.hibernate.basic.entity.heritage.FormationIntra;
import name.luoyong.hibernate.basic.entity.heritage2.Animal;
import name.luoyong.hibernate.basic.entity.heritage2.Chat;
import name.luoyong.hibernate.basic.entity.heritage2.Chien;
import name.luoyong.hibernate.basic.entity.manyToMany.*;
import name.luoyong.hibernate.basic.entity.oneToOne.Address;
import name.luoyong.hibernate.basic.entity.oneToOne.House;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ForeignKey;
import javax.persistence.Persistence;
import java.util.Date;
import java.util.List;

public class Demo {


    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");

    public static void main() {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Post post1 = new Post("JPA with Hibernate");
        Post post2 = new Post("Native Hibernate");

        Tag tag1 = new Tag("Java");
        Tag tag2 = new Tag("Hibernate");

        post1.addTag(tag1);
        post1.addTag(tag2);

        post2.addTag(tag1);

        entityManager.persist(post1);
        entityManager.persist(post2);


        entityManager.getTransaction().commit();
        entityManager.close();

    }

    public static void main2() {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

      /*  Address address = new Address();

        address.setAge(78);
        address.setCodePostal("69800");
        address.setNumero(78);
        address.setVille("Roubaix");
        address.setNomRue("rue de la Lys");

        House house = new House();

        house.setTaille(6778);
        house.setAddress(address);

        entityManager.persist(address);
        entityManager.persist(house);

*/
        House house1 = entityManager.find(House.class, 1L);

        System.out.println(house1.getAddress());

        System.out.println(house1);

        entityManager.getTransaction().commit();
        entityManager.close();

    }


    public static void main3() {


        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Book b1 = new Book("Béthune c'est cool");
        Book b2 = new Book("Le mardi, c'est permis");
        Book b3 = new Book("Un beau livre");
        Book b4 = new Book("Un beau parfait");

       entityManager.persist(b1);
        entityManager.persist(b2);

        Publisher p1 = new Publisher("Marc Antoine");
        Publisher p2 = new Publisher("Sam Samy");

       entityManager.persist(p1);
        entityManager.persist(p2);

        BookPublisher bp1 = new BookPublisher(b1, p1, new Date());
        BookPublisher bp2 = new BookPublisher(b1, p2, new Date());
        BookPublisher bp5 = new BookPublisher(b3, p1, new Date());
        BookPublisher bp6 = new BookPublisher(b4, p1, new Date());
        BookPublisher bp3 = new BookPublisher(b2, p1, new Date());
        BookPublisher bp4 = new BookPublisher(b2, p2, new Date());

        entityManager.persist(bp1);
        entityManager.persist(bp2);
        entityManager.persist(bp3);
        entityManager.persist(bp4);
        entityManager.persist(bp5);
        entityManager.persist(bp6);


        entityManager.getTransaction().commit();
        entityManager.close();

    }


    public static void searchBook() {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Book book = entityManager.find(Book.class, 2);

        System.out.println(book);

        entityManager.remove(book);


        List<Book> books = entityManager.createNativeQuery("select b.name from Book as b join book_publisher as bp on  b.id = bp.book_id where bp.publisher_id = 1").getResultList();

        System.out.println(books);

        entityManager.getTransaction().commit();
        entityManager.close();


    }


    public static void demoHeritage() {

        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();


        Formation formationInter = new FormationInter("Java", 15);
        Formation formationIntra = new FormationIntra("Javascript", "Niveau faible");

        em.persist(formationInter);
        em.persist(formationIntra);

        em.getTransaction().commit();
        em.close();
        entityManagerFactory.close();

    }

    public static void demoHeritage2() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();


        Animal chat = new Chat("mini", "miou");
        Animal chien = new Chien("milou", "dehors");

        em.persist(chat);
        em.persist(chien);

        em.getTransaction().commit();
        em.close();
        entityManagerFactory.close();


    }


}
