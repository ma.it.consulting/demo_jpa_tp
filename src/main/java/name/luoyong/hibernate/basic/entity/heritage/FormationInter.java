package name.luoyong.hibernate.basic.entity.heritage;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("INTER")
public class FormationInter extends Formation{



    private int nbreEntrprises;

    public FormationInter(String theme, int nbreEntrprises) {
        super(theme);
        this.nbreEntrprises = nbreEntrprises;
    }

    public FormationInter(int nbreEntrprises) {
        super();
        this.nbreEntrprises = nbreEntrprises;
    }


}
