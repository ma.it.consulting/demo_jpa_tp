package name.luoyong.hibernate.basic.entity.manyToOne;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="group3")
public class Group {

	private Long id;
	private String name;
	private List<User> users = new LinkedList<User>();
	

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy="group", fetch = FetchType.EAGER)
	public List<User> getUsers() {
		return users;
	}
	

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "Group{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
