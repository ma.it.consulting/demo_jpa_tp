package name.luoyong.hibernate.basic.entity.heritage;


import javax.persistence.*;

@Entity
@DiscriminatorValue("INTRA")
public class FormationIntra extends Formation{

   private String niveau;

   public FormationIntra(){
       super();
   }

    public FormationIntra(String theme,String niveau){
        super(theme);
        this.niveau = niveau;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }
}
