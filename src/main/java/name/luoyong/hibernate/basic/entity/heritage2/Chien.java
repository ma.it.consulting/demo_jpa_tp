package name.luoyong.hibernate.basic.entity.heritage2;

import javax.persistence.Entity;

@Entity
public class Chien extends Animal{

    private String promenade;


    public Chien(){

    }

    public Chien(String promenade) {
        this.promenade = promenade;
    }

    public Chien(String nom, String promenade) {
        super(nom);
        this.promenade = promenade;
    }

    public String getPromenade() {
        return promenade;
    }

    public void setPromenade(String promenade) {
        this.promenade = promenade;
    }
}
